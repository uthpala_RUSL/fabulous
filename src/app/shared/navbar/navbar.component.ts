import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { User } from '../../dataModel/user.model';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { AdminAuth } from '../../_guards/admin.authenticate';
import { StudentAuth } from '../../_guards/stdauth.guard';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  logincred: User;
  showUser=false;
  constructor(private router: Router, private authentication: AuthenticationService,
    private toastr: ToastrService, private adminAuth: AdminAuth, private userAuth: StudentAuth) {
    this.logincred = new User('', '');
  }

  ngOnInit() {
    if (localStorage.getItem('admin')) {
      this.router.navigate(['admin']); 
      //  this.router.navigate(['admin']);  this.router.navigate(['admin']);
      console.log('/admin redirection');
     this. showUser=true;
    } 
    if (localStorage.getItem('student')) {
   this.router.navigate(['activity-page']);
        // this.router.navigate(['activity-page']);
        this. showUser=true;
        
    }
   // this.authentication.logout();
  }
  login() {
    this.authentication.login(this.logincred.username, this.logincred.password).subscribe(result => {
      if (result) {
        this.toastr.success('Login Success');
        // login successful        
        // console.log(this.adminAuth.canActivate());
        if (localStorage.getItem('admin')) {
          this.router.navigate(['admin']); 
          //  this.router.navigate(['admin']);  this.router.navigate(['admin']);
          console.log('/admin redirection');
         this. showUser=true;
        } 
        if (localStorage.getItem('student')) {
       this.router.navigate(['activity-page']);
            // this.router.navigate(['activity-page']);
            this. showUser=true;
            
        }
        

      } else {
        this.toastr.error('Username or password is incorrect');
        this. showUser=!true;
      }
    });

  }

  logout() {
    this.authentication.logout();
    this.logincred = new User('', '');
    this.router.navigate(['/index']);
    this. showUser=!true;
  }
}
