import { NgModule } from '@angular/core';

import { CommonModule, } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { ModuleWithProviders } from '@angular/compiler/src/core';

import { LandingComponent } from './pages/landing/landing.component';
import { FeaturesComponent } from './pages/features/features.component';
import { AboutComponent } from './pages/about/about.component';
import { PricingComponent } from './pages/pricing/pricing.component';
import { AdminComponent } from './pages/admin/admin.component';
import { CreateAccComponent } from './pages/admin/create-acc/create-acc.component';
import { ResultsReportingComponent } from './pages/admin/results-reporting/results-reporting.component';
import { LessonComponent } from './pages/activity-page/lesson/lesson.component';
import { McqComponent } from './pages/activity-page/mcq/mcq.component';




import { StudentAuth } from './_guards/stdauth.guard';
import { ActivityPageComponent } from './pages/activity-page/activity-page.component';
import { AdminAuth } from './_guards/admin.authenticate';

import { EvaluvateComponent } from './pages/admin/evaluvate/evaluvate.component';
const router: Routes = [

  { path: '', redirectTo: 'index', pathMatch: 'full' },
  { path: 'index', component: LandingComponent },
  { path: 'about', component: AboutComponent },
  { path: 'pricing', component: PricingComponent },
  { path: 'features', component: FeaturesComponent },
  {
    path: 'admin', component: AdminComponent, canActivate: [AdminAuth],
    children: [{
      path: '',
      children: [
        { path: '', redirectTo: 'register-student', pathMatch: 'full' },
        { path: 'register-student', component: CreateAccComponent },
        { path: 'result-reporting', component: ResultsReportingComponent },
        { path: 'evaluation-reporting', component: EvaluvateComponent },

      ]
    }]
  },
  { path: 'activity-page', component: ActivityPageComponent, canActivate: [StudentAuth] },
  { path: '**', component: LandingComponent }

];
/*

@NgModule({
  imports: [
    CommonModule,
    BrowserModule],
  exports: [RouterModule]
})
*/
export const routes: ModuleWithProviders = RouterModule.forRoot(router);
