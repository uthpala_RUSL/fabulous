import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LessonRowComponent } from './lesson-row.component';

describe('LessonRowComponent', () => {
  let component: LessonRowComponent;
  let fixture: ComponentFixture<LessonRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LessonRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LessonRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
