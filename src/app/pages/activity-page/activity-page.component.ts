import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-activity-page',
    templateUrl: './activity-page.component.html',
    styleUrls: ['./activity-page.component.scss']
})
export class ActivityPageComponent implements OnInit {
    public chatBoxStateOpen = !true;

    constructor() {
    }

    ngOnInit() {
    }

    chatBoxStateChange() {
        this.chatBoxStateOpen = !this.chatBoxStateOpen;


    }

}
