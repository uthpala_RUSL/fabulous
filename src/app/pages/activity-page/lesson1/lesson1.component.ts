import {Component, OnInit} from '@angular/core';
import {Letter} from './Letter';
import {ActivitiesService} from '../../../services/activities.service';
@Component({
    selector: 'app-lesson1',
    templateUrl: './lesson1.component.html',
    styleUrls: ['./lesson1.component.scss']
})
export class Lesson1Component implements OnInit {
      letterJson: Letter;
      uploaded;
      success=true;

    constructor(private activity: ActivitiesService) {
        this.letterJson = new Letter('', '', '', '');
    }

    ngOnInit() {
    }


    answ() {
        // alert(JSON.stringify(this.letterJson.sender));
        this.uploaded = !this.uploaded;
        let sid = JSON.parse( localStorage.getItem('student'))['username'];
        this.activity.saveActivity1(sid, this.letterJson.sender, this.letterJson.readd, this.letterJson.lbody, this.letterJson.complimant).subscribe(
            res => {
                this.success=false;
            }
        )
    }

    onLetterChange() {

    }

}
