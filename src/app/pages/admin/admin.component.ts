import { Component, OnInit } from '@angular/core';
import { Signup } from '../../dataModel/signup.model';
import { FormsModule } from '@angular/forms';
import { FormControl, FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit  {

  constructor() { }

  ngOnInit() {
  }

}
