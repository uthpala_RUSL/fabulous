import {Component, OnInit} from '@angular/core';
import {EvaluvateService} from '../../../services/evaluvate.service';

import {StudentMarks} from "./studentmarks.model";
@Component({
    selector: 'app-evaluvate',
    templateUrl: './evaluvate.component.html',
    styleUrls: ['./evaluvate.component.scss']
})
export class EvaluvateComponent implements OnInit {
    private s: any;
    private service: any;
    private service2: any;

    activity1;
    activity2;


    constructor(private evaluvateService: EvaluvateService) {
        // this.activity1 = [new StudentMarks(), new StudentMarks(), new StudentMarks()];
        // this.activity2 = [new StudentMarks(), new StudentMarks(), new StudentMarks()];
    }

    ngOnInit() {

        this.evaluvateService.getActivity1().subscribe(res => {
            // console.log(JSON.stringify(res));


            if (res.length < 2) {
                res.push(new StudentMarks());
                res.push(new StudentMarks());
            } else {
                res.push(new StudentMarks());
            }
            this.activity1=res;

            // alert(JSON.stringify(res));

            //  this.activity1 = res;
            /*
             if (res.length < 2) {
             res.res[1].name = "none";
             res[1].SID = "none";
             res[2].name = "none";
             res[2].SID = "none";
             }
             */
            //  alert("Activity 1  :" + JSON.stringify(this.activity1))

        });
        this.evaluvateService.getActivity2().subscribe(res => {
            // console.log(JSON.stringify(res));
            if (res.length < 2) {
                res.push(new StudentMarks());
                res.push(new StudentMarks());
            } else {
                res.push(new StudentMarks());
            }
            this.activity2=res;


        });

    }

}
  


