import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class AdminAuth implements CanActivate,CanActivateChild  {

    canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
        if (localStorage.getItem('admin')) {
            console.log('currentUser found as admin');
            return true;
        }
        this.router.navigate(['/']);
        return false;
    }
    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
        if (localStorage.getItem('admin')) {
            console.log('currentUser found as admin');
            return true;
        }
        this.router.navigate(['/']);
        return false;

    }
}