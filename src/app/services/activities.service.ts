import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions, URLSearchParams} from '@angular/http';
import {Observable} from "rxjs/Observable";
import {Property} from "./property";

@Injectable()
export class ActivitiesService {


    constructor(private http: Http) {
        this.webHost = this.property.getEnv();

    }

    webHost = "http://fabulous-rusl.000webhostapp.com/";
    local = "http://localhost/";

    property = new Property();


    saveActivity1(SID, sender_add, recipient_add, letterBody, complimwntary_close): Observable<Response> {

        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const options = new RequestOptions({headers: headers});
        let body = 'SID=' + SID + '&sender_add=' + sender_add + '&recipient_add='
            + recipient_add + '&body=' + letterBody + '&complimentary_close=' + complimwntary_close;
        return this.http.post(this.webHost + 'php/answer/activity1.php', body, options);
    }

    saveActivity2(SID, silent_k_1, silent_k_2, silent_l_1, silent_l_2, silent_n_1, silent_n_2, silent_s_1, silent_s_2,
                  silent_t_1, silent_t_2): Observable<Response> {
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const options = new RequestOptions({headers: headers});
        let body = 'SID=' + SID +
            '&silent_k_1=' + silent_k_1 + '&silent_k_2=' + silent_k_2 +
            '&silent_l_1=' + silent_l_1 + '&silent_l_2=' + silent_l_2 +
            '&silent_n_1=' + silent_n_1 + '&silent_n_2=' + silent_n_2 +
            '&silent_s_1=' + silent_s_1 + '&silent_s_2=' + silent_s_2 +
            '&silent_t_1=' + silent_t_1 + '&silent_t_2=' + silent_t_2;
        return this.http.post(this.webHost + 'php/answer/activity2.php', body, options);
    }


    getAllUserName(): Observable<Response> {
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const options = new RequestOptions({headers: headers});
        let body = '';
        return this.http.post(this.webHost + 'php/all_users.php', body, options);

    }


    saveMarksForActivity1(uname, marks, remarks): Observable<Response> {
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const options = new RequestOptions({headers: headers});
        let body = 'username=' + uname +
            '&marks=' + marks +
            '&remarks=' + remarks +
            '&activity=activity1';
        console.log('activity 1 saved')
        return this.http.post(this.webHost + 'php/answer/retrive_answers/activity_marks.php', body, options);
    }

    saveMarksForActivity2(uname, marks, remarks): Observable<Response> {

        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const options = new RequestOptions({headers: headers});
        let body = 'username=' + uname +
            '&marks=' + marks +
            '&remarks=' + remarks +
            '&activity=activity2';
        console.log('activity 2 saved')
        return this.http.post(this.webHost + 'php/answer/retrive_answers/activity_marks.php', body, options);
    }


    retriveActivity1(uname, activity): Observable<Response> {
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const options = new RequestOptions({headers: headers});
        let body = 'username=' + uname +
            '&activity=' + activity;
        console.log('retrive ' + activity + ' saved')
        return this.http.post(this.webHost + 'php/answer/retrive_answers/retrive_activity1.php', body, options);
    }


}
