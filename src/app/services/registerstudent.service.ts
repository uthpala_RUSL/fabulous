import { Injectable, ViewContainerRef } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { ToastrService } from 'ngx-toastr';
import {Property} from "./property";

@Injectable()
export class RegisterStudent {

    webHost="http://fabulous-rusl.000webhostapp.com/";
    local="http://localhost/";

    property = new Property();


    constructor(private http: Http, private toastr: ToastrService) {
        this.webHost=this.property.getEnv();

    }
    studentReg(username, password, school, studentid, firstname, lastname, dob): Observable<Response> {

        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const options = new RequestOptions({ headers: headers });
        let body = 'username=' + username + '&password=' + password + '&school='
            + school + '&studentid=' + studentid + '&firstname=' + firstname + '&lastname=' + lastname + '&dob=' + dob;
        return this.http.post(this.webHost+'php/registerstudent.php', body, options)
           /*.subscribe(data => {
                this.toastr.success('New Record Added!', 'Success!');

                return true;
            },
                error => {
                    this.toastr.error('Error Occured', 'Alert!');
                    return false;

                }
            )*/;
    }
    checkusername(username): Observable<Response> {
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const options = new RequestOptions({ headers: headers });
        let search = new URLSearchParams();
        search.set('username', username);
        return this.http.post(this.webHost+'php/checkusername.php', search, options)


            ;
    }



}