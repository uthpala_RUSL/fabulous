import {Injectable} from '@angular/core';
import {Http, Headers, Response, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/map'
import {Property} from "./property";

@Injectable()
export class AuthenticationService {
    public token: string;

    webHost = "http://fabulous-rusl.000webhostapp.com/";
    local = "http://localhost/";


    property = new Property();


    constructor(private http: Http) {
        // set token if saved in local storage
        this.webHost=this.property.getEnv();

        if (localStorage.getItem('admin')) {
            var currentUser = JSON.parse(localStorage.getItem('admin'));
            this.token = currentUser && currentUser.token;
        } else if (localStorage.getItem('student')) {
            var currentUser = JSON.parse(localStorage.getItem('student'));
            this.token = currentUser && currentUser.token;
        }

    }

    login(username: string, password: string): Observable<boolean> {
        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        const options = new RequestOptions({headers: headers});
        let search = new URLSearchParams();
        search.set('username', username);
        search.set('password', password);
        let body = 'username=' + username + '&password=' + password;
        return this.http.post(this.webHost + 'php/logingauth.php', body, options)
            .map((response: Response) => {
                let token = response.json() && response.json().token;
                if (token) {
                    // set token property
                    this.token = token;
                    if (username.toString().trim() === 'AdminUser') {
                        localStorage.setItem('admin', JSON.stringify({username: username, token: token}));
                        console.log("Admin has logged in");
                        return true;
                    } else {
                        localStorage.setItem('student', JSON.stringify({username: username, token: token}));
                        console.log("Student has logged in");
                        return true;
                    }
                    // return true to indicate successful login

                } else {
                    // return false to indicate failed login
                    return false;
                }
            });
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('admin');
        localStorage.removeItem('student');

    }
}