-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 13, 2018 at 08:49 PM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fabulous`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity1`
--

DROP TABLE IF EXISTS `activity1`;
CREATE TABLE IF NOT EXISTS `activity1` (
  `ID` int(50) NOT NULL AUTO_INCREMENT,
  `SID` varchar(50) NOT NULL,
  `sender_add` varchar(1000) NOT NULL,
  `recipient_add` varchar(1000) NOT NULL,
  `body` varchar(2000) NOT NULL,
  `complimentary_close` varchar(300) NOT NULL,
  `mark` int(4) DEFAULT NULL,
  `remarks` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `activity2`
--

DROP TABLE IF EXISTS `activity2`;
CREATE TABLE IF NOT EXISTS `activity2` (
  `ID` int(50) NOT NULL AUTO_INCREMENT,
  `SID` varchar(50) NOT NULL,
  `silent_s_1` varchar(100) NOT NULL,
  `silent_s_2` varchar(100) NOT NULL,
  `silent_l_1` varchar(100) NOT NULL,
  `silent_l_2` varchar(100) NOT NULL,
  `silent_k_1` varchar(100) NOT NULL,
  `silent_k_2` varchar(100) NOT NULL,
  `silent_n_1` varchar(100) NOT NULL,
  `silent_n_2` varchar(100) NOT NULL,
  `silent_t_1` varchar(100) NOT NULL,
  `silent_t_2` varchar(100) NOT NULL,
  `mark` int(4) DEFAULT NULL,
  `remarks` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=123456798 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `usertable`
--

DROP TABLE IF EXISTS `usertable`;
CREATE TABLE IF NOT EXISTS `usertable` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SID` varchar(50) DEFAULT NULL,
  `FirstName` varchar(20) DEFAULT NULL,
  `LastName` varchar(20) DEFAULT NULL,
  `UserName` varchar(20) DEFAULT NULL,
  `Password` varchar(20) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `School` varchar(25) DEFAULT NULL,
  `roleType` varchar(100) DEFAULT NULL,
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usertable`
--

INSERT INTO `usertable` (`ID`, `SID`, `FirstName`, `LastName`, `UserName`, `Password`, `DOB`, `School`, `roleType`) VALUES
(11, 'da12345', 'danu', '', 'danushka', 'Danushka1234', '2018-03-12', 'soon', NULL),
(10, 'soon1234', 'danu', '', 'chethana', 'dA1234', '2018-03-10', 'soom', NULL),
(12, NULL, NULL, NULL, 'Admin', 'password', NULL, NULL, NULL),
(13, NULL, NULL, NULL, 'AdminUser', 'password', NULL, NULL, 'ADMIN'),
(14, 'msc123', 'Viroj', '', 'virojfernando', 'Vt1234', '2018-05-24', 'MSC', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
